

<?php
	include('../users/timeout.php');
	include('../users/connection.php');
	if($login_session!="CMRentadmin"){
		echo "<script type='text/javascript'>  window.location='../users/login.php'; </script>"; 
	}
?>


<!DOCTYPE html>
<html>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta HTTP-EQUIV="Content-Language" Content="hu">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="../res/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel = "stylesheet" href = "../css/stylepage1.css"/>
	<link rel = "stylesheet" href = "../css/modal.css"/>
	<link rel = "stylesheet" href = "../css/admin.css"/>
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="https://fonts.googleapis.com/css?family=Raleway:300" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" 
	rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	
	<head>
		<title>Admin kezdőlap</title>
		<link type="text/css" rel="stylesheet" href="../css/style1.css"/>
		<script type="text/javascript" src="../javascript/jquery.js"></script>
		<script type="text/javascript" src="../javascript/passwordStrengthMeter.js"></script>
		<script type="text/javascript" src="../javascript/adminpage.js"></script>
	</head>
	
	<!-- body -->
	
	<body>	
		<div id="bgStyle"></div>
			<div class="navbar">
			  <a href="adminindex.php"><i class="fa fa-home fa-2x"></i></a>
			  <a href="adminmessages.php"><i class="fa fa-envelope fa-2x"></i></a>
			  
			  <div class="dropdown">
				<button class="dropbtn" onclick="myFunction()"><i class="fa fa-car fa-2x"></i>
				  <i class="fa fa-caret-down"></i>
				</button>
				<div class="dropdown-content" id="myDropdown">
				  <a href="carupload.php">Személygépkocsi felvétele</a>
				  <a href="carmodify.php">Személygépkocsi módosítása</a>
				  <a href="cardelete.php">Személygépkocsi törlése</a>
				</div>
			  </div>
			  
			   <div class="dropdown">
				<button class="dropbtn" onclick="myFunction1()"><i class="fa fa-motorcycle fa-2x"></i>
				  <i class="fa fa-caret-down"></i>
				</button>
				<div class="dropdown-content" id="myDropdown1">
				  <a href="motorupload.php">Motor felvétele</a>
				  <a href="motormodify.php">Motor módosítása</a>
				  <a href="motordelete.php">Motor törlése</a>
				</div>
			  </div> 
			  
			  <div class="dropdown">
				<button class="dropbtn" onclick="myFunction2()" ><i class="fa fa-user fa-2x"></i>
				  <i class="fa fa-caret-down"></i>
				</button>
				<div class="dropdown-content" id="myDropdown2">
				  <a href="allusers.php">Összes felhasználó</a>
				  <a href="onlineusers.php">Online felhasználók</a>
				  <a href="carrents.php">Autós kölcsönzések</a>
				  <a href="motorrents.php">Motoros kölcsönzések</a>
				</div>
			  </div>
			  
			  <div class="dropdown">
				<button class="dropbtn" onclick="myFunction3()" ><i class="fa fa-star fa-2x"></i>
				  <i class="fa fa-caret-down"></i>
				</button>
				<div class="dropdown-content" id="myDropdown3">
				  <a href="carratings.php">Autós értékelések</a>
				  <a href="motorratings.php">Motoros értékelések</a>
				</div>
			  </div>
			  <a align = "right" title = "Automatikus kijelentkezés <?php echo $timeleft; ?> mp múlva."
			  href= "" style = "padding: 0;margin-top: 1.2%; margin-left: 41%;font-size: 22px; text-align: right;"><?php echo $timeleft; ?> mp</a>
			  <form method = "POST" action = "../users/logout.php" enctype = "multipart/form-data" name = "logout">
				<input type = "submit" class = "input" name = "logout" value = "Kijelentkezés" style = "align: right;"/> <!--<i class="fas fa-sign-out-alt"></i> -->
			  </form>
			  
			  
			</div>
			
			
			</br></br>
			
			<div align = "center">		
				<div align = "center" id = "cars">
					<table align = "center" width = "90%" id = "carmotortable" style = "background: gray; margin-left: -5px;"cellpadding = "0" cellspacing = "0" >
						<tr>
							<td align = "left" style = "padding: 10px; color: white; font-size: 41px;
							text-shadow: 1px 1px 2px black, 0 0 25px white, 0 0 5px white;">Legújabb autók az adatbázisban</td>
						</tr>
					</table>
				</div>
			</div>
			
			<div align = "center">
			<?php $cars = "SELECT * FROM auto ORDER BY evjarat;";
					 $getCars = mysqli_query($conn, $cars);
					 
					 if(mysqli_num_rows($getCars)>0){
						 while($row = mysqli_fetch_assoc($getCars)){ ?>
		
							  <table id = "carmotortable" style="display: inline-block; width: 45%; margin-left: -6px; border-right: 1px solid gray;" align = "center" cellspacing = "0" cellpadding = "0">
								<tr>
									<td style = "text-align: right;" width = "15%">Kategória:</td>
									<td style = "text-align: left; padding-left: 10px;"width = "25%"><?php echo htmlspecialchars($row['kategoria']); ?></td>
									<td rowspan = "10" style = "padding-left: 10px;">
										<img src = "<?php echo $row["fenykep"]; ?>" style = "width:100%; height: 200px; border-radius: 10px;" />
									</td>
								</tr>
								<tr>
									<td style = "text-align: right;" width = "15%">Autómárka:</td>
									<td style = "text-align: left; padding-left: 10px;"width = "25%"><?php echo htmlspecialchars($row['automarka_id']); ?></td>
								</tr>
								<tr>
									<td style = "text-align: right;" width = "15%">Márka típusa:</td>
									<td style = "text-align: left; padding-left: 10px;"width = "25%"><?php echo htmlspecialchars($row['marka_tipus']); ?></td>
								</tr>
								<tr>
									<td style = "text-align: right;" width = "15%">Teljesítmény:</td>
									<td style = "text-align: left; padding-left: 10px;"width = "25%"><?php echo htmlspecialchars($row['teljesitmeny']); ?> LE</td>
								</tr>
								<tr>
									<td style = "text-align: right;" width = "15%">Száll. személyek:</td>
									<td style = "text-align: left; padding-left: 10px;"width = "25%"><?php echo htmlspecialchars($row['szallithato_szemelyek']); ?> fő</td>
								</tr>
								<tr>
									<td style = "text-align: right;" width = "15%">Üzemanyag:</td>
									<td style = "text-align: left; padding-left: 10px;"width = "25%"><?php echo htmlspecialchars($row['uzemanyag']); ?></td>
								</tr>
								<tr>
									<td style = "text-align: right;" width = "15%">Hengerűrtartalom:</td>
									<td style = "text-align: left; padding-left: 10px;"width = "25%"><?php echo htmlspecialchars($row['hengerurtartalom']); ?> cm<sup>3</sup></td>
								</tr>
								<tr>
									<td style = "text-align: right;" width = "15%">Ár (1-6 napig): </td>
									<td style = "text-align: left; padding-left: 10px;"width = "25%"><?php echo htmlspecialchars($row['ar_1']); ?> HUF</td>
								</tr>
								<tr>
									<td style = "text-align: right;" width = "15%">Ár (7-30 napig):</td>
									<td style = "text-align: left; padding-left: 10px;"width = "25%"><?php echo htmlspecialchars($row['ar_2']); ?> HUF</td>
								</tr>
								<tr>
									<td style = "text-align: right;" width = "15%">Ár (31-365 napig):</td>
									<td style = "text-align: left; padding-left: 10px;"width = "25%"><?php echo htmlspecialchars($row['ar_3']); ?> HUF</td>
								</tr>
								<tr>
									<td style = "text-align: right;" width = "15%">Szín:</td>
									<td style = "text-align: left; padding-left: 10px;"width = "25%"><?php echo htmlspecialchars($row['autoszin']); ?> </td>
								</tr>
								<tr>
									<td style = "text-align: right;" width = "15%">Átlagfogyasztás:</td>
									<td style = "text-align: left; padding-left: 10px;"width = "25%"><?php echo htmlspecialchars($row['atlagfogyasztas']); ?> l/100 km</td>
									<td rowspan = "3">
									<form method = "get" action = "carmodifyChosen.php" enctype = "multipart/form-data" name = "viewcar">
										<?php echo '<input type = "submit"  class = "viewvehicle" value = "Autó adatlapja" style = "height: 40px;width: 330px; padding: 0; "name = "'.$row["id"].'" >
									</form>'?>
									</td>
								</tr>
								<tr>
									<td style = "text-align: right;" width = "15%">Gyorsulás (0-100):</td>
									<td style = "text-align: left; padding-left: 10px;"width = "25%"><?php echo htmlspecialchars($row['gyorsulas']); ?> mp</td>
								</tr>
								
							  </table>

							 
						<?php
						 }
					 }
					 
				?>
			
			</div>
			</br></br>
	</body>
</html>